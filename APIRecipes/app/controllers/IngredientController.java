package controllers;

import java.util.List;

import javax.inject.Inject;

import com.fasterxml.jackson.databind.JsonNode;

import controllers.ReponseObject.JsonResponseRequest;
import io.ebean.PagedList;
import models.Ingredients;
import play.Logger;
import play.cache.SyncCacheApi;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import utils.Globals;

/**
 * Ingredient controller class
 * 
 * @author eladiosuarez and nacho garcia
 *
 */
public class IngredientController extends Controller {
	@Inject
	FormFactory formFactory;
	@Inject
	private SyncCacheApi cache;

	/**
	 * Save ingredient in the database with the method checkAndSaveIngredient.
	 * Include session with the ingredient id.
	 * 
	 * @return result value.
	 */
	public Result addIngredient() {

		Form<Ingredients> f = formFactory.form(Ingredients.class).bindFromRequest();
		if (f.hasErrors()) {
			return Results.status(Globals.codeNotAcceptable, JsonResponseRequest.printMessage(f.errorsAsJson().toString(), Globals.codeNotAcceptable));
		}

		Ingredients ingredient = f.get();
		Logger.debug(ingredient.toString());

		if (ingredient.checkAndSaveIngredient(ingredient)) {

			session().put(Globals.ID, ingredient.getId().toString());

			return Results.status(Globals.codeCreated, JsonResponseRequest.printMessage(Globals.KeySucces, Globals.codeCreated));

		} else {

			return Results.status(409,
					JsonResponseRequest.printMessage(Globals.KeyIngredientRepeated, Globals.codeRepited));
		}

	}

	/**
	 * Retrieve the ingredient by name. Save in cache the ingredient with the
	 * function getOrElseUpdate. Return the result in xml and json format.
	 * 
	 * @param name
	 *            recipe name.
	 * @return result request.
	 */
	public Result retrieveIngredient(String name) {

		String key = Globals.KEY_INGREDIENT + name;

		Ingredients ingredient = cache.getOrElseUpdate(key, () -> {

			return Ingredients.findByName(name);

		}, 60 * 60);

		if (ingredient == null) {
			return Results.status(Globals.codeNoExist,
					JsonResponseRequest.printMessage(Globals.KeyNoExistIngredient, Globals.codeNoExist));
		}

		if (request().accepts(Globals.XML)) {

			Logger.debug("xml format");
			return ok(views.xml.ingredient.render(ingredient));

		} else if (request().accepts(Globals.JSON)) {

			Logger.debug("json format");
			String keyView = Globals.KEY_VIEW + name;
			JsonNode json = cache.get(keyView);
			if (json == null) {
				json = Json.toJson(ingredient);
				cache.set(keyView, json);

			}

			Logger.debug("ingredient json" + json.toString());
			return ok(json);
		} else

			return Results.status(Globals.codeUnsupported,
					JsonResponseRequest.printMessage(Globals.KeyUnsupported, Globals.codeUnsupported));
	}

	/**
	 * Update the ingredient by name. Cache: to get, if exist, the object ingredient
	 * 
	 * @param name
	 *            ingredient name.
	 * @return Result request.
	 */
	public Result updateIngredient(String name) {

		Form<Ingredients> f = formFactory.form(Ingredients.class).bindFromRequest();
		String key = Globals.KEY_INGREDIENT + name;
		String keyView = Globals.KEY_VIEW + name;
		cache.remove(keyView);
		Ingredients ingredientWS = f.get();

		if (ingredientWS != null) {

			Ingredients ingBBDD = Ingredients.findByName(ingredientWS.getName());

			if (!name.equalsIgnoreCase(ingredientWS.getName()) && ingBBDD != null) {

				return Results.status(Globals.codeRepited, JsonResponseRequest.printMessage(Globals.KeyExistIngredient, Globals.codeRepited));

			} else if (Ingredients.findByName(name) == null) {

				return Results.status(Globals.codeNoExist,
						JsonResponseRequest.printMessage(Globals.KeyNoExistIngredient, Globals.codeNoExist));
			} else {
				Ingredients ingredient = cache.getOrElseUpdate(key, () -> {
					return Ingredients.findByName(name);
				}, 60 * 60);

				Long idIngredient = ingredient.getId();
				ingredient = ingBBDD;
				cache.remove(key);
				ingredient = ingredientWS;
				ingredient.setId(idIngredient);
				ingredient.update();

				return Results.status(Globals.codeSucces, JsonResponseRequest.printMessage(Globals.KeyIngredientUpdated, Globals.codeSucces));
			}
		} else {
			return Results.status(Globals.codeNoExist,
					JsonResponseRequest.printMessage(Globals.KeyNoExistIngredient, Globals.codeNoExist));
		}

	}

	/**
	 * Get list ingredients with pagination.
	 * 
	 * @param page
	 *            number page.
	 * @return result ingredients.
	 */
	public Result retrieveIngredients(Integer page) {

		PagedList<Ingredients> pageIngredients = Ingredients.findAll(page);
		List<Ingredients> ingredients = pageIngredients.getList();
		Integer count = pageIngredients.getTotalCount();
		if (!ingredients.isEmpty()) {

			if (request().accepts(Globals.XML)) {

				return ok(views.xml.ingredients.render(ingredients)).withHeader(Globals.HEADER_COUNT, count.toString());
			} else if (request().accepts(Globals.JSON)) {

				return ok(Json.toJson(ingredients)).withHeader(Globals.HEADER_COUNT, count.toString());
			}
			return Results.status(Globals.codeUnsupported,
					JsonResponseRequest.printMessage(Globals.KeyUnsupported, Globals.codeUnsupported));
		} else
			return Results.status(Globals.codeNoExist, JsonResponseRequest.printMessage(Globals.KeyNoIngredients, Globals.codeNoExist));
	}
}
