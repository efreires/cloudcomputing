package controllers;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.fasterxml.jackson.databind.JsonNode;

import controllers.ReponseObject.JsonResponseRequest;
import io.ebean.PagedList;
import io.ebean.enhance.common.SysoutMessageOutput;
import models.Ingredients;
import models.Recipes;
import play.Logger;
import play.cache.SyncCacheApi;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import utils.Globals;

/**
 * Class recipe controller.
 * 
 * @author eladiosuarez, nachogarcia
 *
 */
public class RecipesController extends Controller {
	@Inject
	FormFactory formFactory;
	@Inject
	private SyncCacheApi cache;

	/**
	 * Save the new recipe and session.
	 * 
	 * @return operation result.
	 */
	public Result addRecipe() {
		Form<Recipes> f = formFactory.form(Recipes.class).bindFromRequest();
		if (f.hasErrors()) {
			return Results.status(Globals.codeNotAcceptable, JsonResponseRequest.printMessage(Globals.KeySucces, Globals.codeNotAcceptable));
		}
		Recipes recip = f.get();
		Logger.debug(recip.toString());

		if (recip.checkAndSaveRecipe(recip)) {
			session().put(Globals.ID, recip.getId().toString());
			return Results.status(Globals.codeCreated, JsonResponseRequest.printMessage(Globals.KeySucces, Globals.codeCreated));
		} else {
			return Results.status(Globals.codeRepited, JsonResponseRequest.printMessage(Globals.KeyRepited, Globals.codeRepited));
		}
	}

	/**
	 * Get the recipe result with a name. If the recipe exist in the cache return
	 * this recipe, if no exist get the recipe from the database.
	 * 
	 * @param name
	 *            recipe name
	 * @return recipe result.
	 */
	public Result retrieveRecipe(String name) {
		Logger.info("Obteniendo receta " + name);
		String key = Globals.KEY_RECIPE + name;
		Recipes recipe = Recipes.findByName(name);
		if (recipe == null) {
			return Results.status(Globals.codeNoExist, JsonResponseRequest.printMessage(Globals.KeyNoExist, Globals.codeNoExist));
		}
		Logger.debug("Recipe " + recipe.toString() + "     " + recipe.getPreparation());

		if (request().accepts(Globals.XML)) {
			Logger.debug("xml format");

			return ok(views.xml.recipe.render(recipe));
		} else if (request().accepts(Globals.JSON)) {
			Logger.debug("json format");
			JsonNode json = Json.toJson(recipe);
			Logger.debug("Recipe json" + json.toString());
			return ok(json);
		} else
			return Results.status(Globals.codeUnsupported,
					JsonResponseRequest.printMessage(Globals.KeyUnsupported, Globals.codeUnsupported));

	}

	/**
	 * Get recipes by name of ingredient.
	 * 
	 * @param ingredient
	 *            ingredient name.
	 * @return recipes.
	 */
	public Result retrieveRecipeIngredient(String ingredient) {
		List<Recipes> recipes = new ArrayList<>();
		recipes = Recipes.searchByIngredientName(ingredient);
		Logger.info("Obteniendo receta por ingrediente " + ingredient);

		if (recipes != null) {
			if (request().accepts(Globals.XML)) {

				return ok(views.xml.recipes.render(recipes));
			} else if (request().accepts(Globals.JSON)) {
				return ok(Json.toJson(recipes));
			}
			return ok(Json.toJson(recipes));

		} else {
			Logger.info("No existe receta");
			return Results.status(Globals.codeNoExist, JsonResponseRequest.printMessage(Globals.KeyNoExist, Globals.codeNoExist));

		}
	}

	/**
	 * Update recipe by name.
	 * 
	 * @param name
	 *            recipe name.
	 * @return result recipe operation.
	 */
	public Result updateRecipe(String name) {

		Form<Recipes> f = formFactory.form(Recipes.class).bindFromRequest();
		String key = Globals.KEY_RECIPE + name;
		String keyView = Globals.KEY_VIEW + name;
		cache.remove(keyView);
		Recipes recipebody = f.get();
		/**
		 * If the name is different and the new name exist the recipe doesn't update
		 */
		if (recipebody != null) {

			if (!name.equalsIgnoreCase(recipebody.getName()) && Recipes.findByName(recipebody.getName()) != null
					&& Recipes.findByName(name) != null) {
				return Results.status(Globals.codeRepited, JsonResponseRequest.printMessage(Globals.KeyExist, Globals.codeRepited));
			} else if (Recipes.findByName(name) == null) {
				return Results.status(Globals.codeNoExist, JsonResponseRequest.printMessage(Globals.KeyNoExist, Globals.codeNoExist));

			} else {
				Recipes recipe = cache.getOrElseUpdate(key, () -> {
					return Recipes.findByName(name);
				}, 60 * 60);

				Long idRecipe = recipe.getId();

				recipe = recipebody;
				recipe.setId(idRecipe);
				cache.remove(key);
				cache.set(key, recipe);
				recipe.update();
				return ok(JsonResponseRequest.printMessage(Globals.KeyUpdated, Globals.codeSucces));
			}
		} else {
			return Results.status(Globals.codeNoExist, JsonResponseRequest.printMessage(Globals.KeyNoExist, Globals.codeNoExist));

		}
	}

	/**
	 * Delete recipe by name.
	 * 
	 * @param recipeName
	 *            name recipe.
	 * @return result operation.
	 */
	public Result deleteRecipe(String recipeName) {
		Logger.debug("Borrando recetas " + recipeName);
		Recipes recipe = Recipes.findByName(recipeName);
		if (recipe != null) {
			cache.remove(Globals.KEY_RECIPE + recipeName);
			Logger.debug("Borrando receta " + recipeName);
			if (recipe.delete()) {

				return ok(JsonResponseRequest.printMessage(Globals.KeyDeleted, Globals.codeSucces));

			} else {
				return Results.status(Globals.codeInternalServer,
						JsonResponseRequest.printMessage(Globals.KeyServerError, Globals.codeInternalServer));

			}
		} else {
			return Results.status(Globals.codeNoExist, JsonResponseRequest.printMessage(Globals.KeyNoExist, Globals.codeNoExist));
		}
	}

	/**
	 * Get all recipes with pagination. The first page is the number 0.
	 * 
	 * @return all recipes.
	 */
	public Result retrieveListRecipes(Integer page) {

		PagedList<Recipes> pagedListRecipes = Recipes.findAll(page);
		List<Recipes> listRecipes = pagedListRecipes.getList();
		Integer count = pagedListRecipes.getTotalCount();
		if (!listRecipes.isEmpty()) {

			if (request().accepts(Globals.XML)) {

				return ok(views.xml.recipes.render(listRecipes)).withHeader(Globals.HEADER_COUNT, count.toString());
			} else if (request().accepts(Globals.JSON)) {
				return ok(Json.toJson(listRecipes)).withHeader(Globals.HEADER_COUNT, count.toString());
			}
			return Results.status(Globals.codeUnsupported,
					JsonResponseRequest.printMessage(Globals.KeyUnsupported, Globals.codeUnsupported));

		} else {
			Logger.debug("No Existe");
			return Results.status(Globals.codeNoExist, JsonResponseRequest.printMessage(Globals.KeyNoRecipes, Globals.codeNoExist));
		}

	}
}
