package controllers.ReponseObject;

import com.fasterxml.jackson.databind.JsonNode;

import play.i18n.Messages;
import play.libs.Json;
import play.mvc.Http;

public class JsonResponseRequest {

	private String message;
	private int code;

	public JsonResponseRequest(int code, String message) {
		super();
		this.message = message;
		this.code = code;
	}

	public JsonNode toJson() {
		return Json.toJson(this);
	}

	public String getMessage() {
		return message;
	}

	public int getCode() {
		return code;
	}

	/**
	 * Print the status response message.
	 * @param keyMessage
	 *            i18n key.
	 * @param messageCode
	 *            i18n code.
	 * @return Json with the message response
	 */
	public static JsonNode printMessage(String keyMessage, int messageCode) {
		Messages messages = Http.Context.current().messages();

		return new JsonResponseRequest(messageCode, messages.at(keyMessage)).toJson();

	}

}
