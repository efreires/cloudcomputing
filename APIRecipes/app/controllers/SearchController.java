package controllers;

import java.util.List;

import controllers.ReponseObject.JsonResponseRequest;
import models.Ingredients;
import models.Recipes;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import play.mvc.Results;
import utils.Globals;

/**
 * Class Search Controller for search by values and criteria.
 * 
 * @author eladiosuarez and nacho garcia
 *
 */
public class SearchController extends Controller {

	/**
	 * Search ingredients and recipes by attribute (calories, conservation time or diners)
	 * criteria (less, greater) and values (search number value).
	 * 
	 * Example:
	 * http://localhost:9000/search?typeObject=recipes&attribute=calories&criteria=less&value=55500
	 * 
	 * @return result recipes or ingredients
	 */
	public Result searchByCriteria() {

		String typeObj = request().getQueryString(Globals.TYPE_OBJECT);
		String attr = request().getQueryString(Globals.ATTRIBUTE);
		String criteria = request().getQueryString(Globals.CRITERIA);
		String valueToSearch = request().getQueryString(Globals.VALUE);

		if (!validateData(valueToSearch))
			return Results.status(415,
					JsonResponseRequest.printMessage(Globals.KeyUnsupported, Globals.codeUnsupported));

		String wsCriteria = getCriteria(criteria);
		if (wsCriteria.equalsIgnoreCase(Globals.KeyUnsupported))
			return Results.status(Globals.codeNotImplemented,
					JsonResponseRequest.printMessage(Globals.keyNotImplemented, Globals.codeNotImplemented));
		if (!validateColumns(attr.toUpperCase()))
			return Results.status(Globals.codeUnsupported,
					JsonResponseRequest.printMessage(Globals.KeySearchTypeNotSupported, Globals.codeUnsupported));

		if (attr.equalsIgnoreCase(Globals.DINERS) && typeObj.equalsIgnoreCase(Globals.INGREDIENTS))
			return Results.status(Globals.codeUnsupported,
					JsonResponseRequest.printMessage(Globals.KeySearchTypeNotSupported, Globals.codeUnsupported));

		if (typeObj.equalsIgnoreCase(Globals.INGREDIENTS)) {
			List<Ingredients> ingredients = Ingredients.findByAttr(attr, getCriteria(criteria), valueToSearch);
			if (request().accepts(Globals.XML)) 
			{
				return ok(views.xml.ingredients.render(ingredients));
			} else if(request().accepts(Globals.JSON))
			{
				return ok(Json.toJson(ingredients));
			}
			
			return Results.status(Globals.codeUnsupported,
					JsonResponseRequest.printMessage(Globals.KeyUnsupported, Globals.codeUnsupported));
			
		} else if (typeObj.equalsIgnoreCase(Globals.RECIPES)) {
			List<Recipes> recipes = Recipes.findByAttribute(attr, wsCriteria, valueToSearch);
			if (request().accepts(Globals.XML)) 
			{
				return ok(views.xml.recipes.render(recipes));
			} else if(request().accepts(Globals.JSON))
			{
				return ok(Json.toJson(recipes));				
			}
			return Results.status(Globals.codeUnsupported,
					JsonResponseRequest.printMessage(Globals.KeyUnsupported, Globals.codeUnsupported));
		} else 
		{
			return Results.status(Globals.codeUnsupported,
					JsonResponseRequest.printMessage(Globals.KeyUnsupportedSearch, Globals.codeUnsupported));
		}
	}

	/**
	 * Get "<" ">" by criteria
	 * 
	 * @param criteria
	 *            criteria
	 * @return string with the operator.
	 */
	private String getCriteria(String criteria) {
		switch (criteria) {
		case "less":
			return "<";
		case "greater":
			return ">";
		default:
			return Globals.KeyUnsupported;
		}
	}

	/**
	 * Check if data is an Integer.
	 * 
	 * @param value
	 *            dataValue
	 * @return true o false
	 */
	public static boolean validateData(String value) {
		try {
			Integer.valueOf(value);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * Check the attributes values cloums.
	 * 
	 * @param attr
	 *            attribute type.
	 * @return boolean
	 */
	public static boolean validateColumns(String attr) {
		switch (attr) {
		case Globals.CALORIES:
			return true;
		case Globals.CONSERVATION_TIME:
			return true;
		case Globals.DINERS:
			return true;
		default:
			return false;
		}
	}
}
