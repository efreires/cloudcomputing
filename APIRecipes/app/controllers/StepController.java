package controllers;

import javax.inject.Inject;

import controllers.ReponseObject.JsonResponseRequest;
import models.Steps;
import play.data.FormFactory;
import play.mvc.Controller;
import play.data.Form;
import play.mvc.Result;
import play.mvc.Results;
import utils.Globals;

public class StepController extends Controller {

	@Inject
	FormFactory formFactory;

	/**
	 * Update the value step of recipe.
	 * 
	 * @return result test.
	 */
	public Result updateStepDescription() {
		Form<Steps> f = formFactory.form(Steps.class).bindFromRequest();
		Steps steps = f.get();
		if (!Steps.updateStep(steps)) {
			return Results.status(Globals.codeNoExist, JsonResponseRequest.printMessage(Globals.KeyStepNoExist, Globals.codeNoExist));
		}
		return Results.status(Globals.codeSucces, JsonResponseRequest.printMessage(Globals.KeySucces, Globals.codeSucces));

	}

}
