package models;

import java.sql.Timestamp;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import io.ebean.annotation.CreatedTimestamp;
import io.ebean.annotation.UpdatedTimestamp;
@MappedSuperclass
public class BaseModel extends io.ebean.Model {

	@Id
	public Long id;
	@Version
	Long version;
	@CreatedTimestamp
	Timestamp whenCreated;
	@UpdatedTimestamp
	Timestamp whenUpdated;

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public Timestamp getWhenCreated() {
		return whenCreated;
	}

	public void setWhenCreated(Timestamp whenCreated) {
		this.whenCreated = whenCreated;
	}

	public Timestamp getWhenUpdated() {
		return whenUpdated;
	}

	public void setWhenUpdated(Timestamp whenUpdated) {
		this.whenUpdated = whenUpdated;
	}

}
