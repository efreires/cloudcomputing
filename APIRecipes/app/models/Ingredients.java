package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import io.ebean.Finder;
import io.ebean.Model;
import io.ebean.PagedList;
import play.data.validation.Constraints.Required;
import validators.CheckString;

@Entity
public class Ingredients extends Model {
	@Id
	private Long id;

	@Required
	private String name;

	@CheckString
	private String color;

	@CheckString
	private String typeIngredient;

	@CheckString
	private String description;

	@Required
	private String quantity = "";

	@Required
	private Integer calories;

	private Long conservationTime;

	@ManyToMany(cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<Varieties> varieties;

	@ManyToMany(mappedBy = "ingredients")
	@JsonBackReference
	private List<Recipes> recipes = new ArrayList<>();

	@Transient
	private List<Varieties> varietyIngredientList = new ArrayList<>();

	@Transient
	private List<String> ingredientVarietyNameAddedList = new ArrayList<>();
	

	public Ingredients() {
		super();
	}

	public static final Finder<Long, Ingredients> findIngredients = new Finder<>(Ingredients.class);

	public Ingredients(@Required String name, String color, String typeIngredient, String description, String quantity,
			Integer calories, Long conservationTime, List<Varieties> varieties) {
		super();
		this.name = name;
		this.color = color;
		this.typeIngredient = typeIngredient;
		this.description = description;
		this.quantity = quantity;
		this.calories = calories;
		this.conservationTime = conservationTime;
		this.varieties = varieties;
	}

	/**
	 * Find ingredient by number of calories.
	 * 
	 * @param calories
	 *            Calories.
	 * @return List<Ingredients>.
	 */
	public static List<Ingredients> findByAttr(String attr, String criteria, String val1) {
		if (criteria.equals("<"))
			return findIngredients.query().where().le(attr.toUpperCase(), val1).findList();
		else
			return findIngredients.query().where().gt(attr.toUpperCase(), val1).findList();
	}

	/**
	 * Find ingredient by name.
	 * 
	 * @param name
	 *            name.
	 * @return ingredient.
	 */
	public static Ingredients findByName(String name) {
		return findIngredients.query().where().eq("NAME", name).findOne();
	}

	/**
	 * Save in database the ingredient if no exist.
	 * 
	 * @param ingredient
	 *            ingredient.
	 * @return boolean.
	 */
	public boolean checkAndSaveIngredient(Ingredients ingredient) {
		List<Varieties> varitiesIngredient = new ArrayList<>();
		if (Ingredients.findByName(ingredient.name) == null) {
			varitiesIngredient.addAll(ingredient.getVarieties());
			checkAndAddVarieties(varitiesIngredient);
			this.setVarieties(varietyIngredientList);
			this.save();
			return true;
		}
		return false;
	}

	/**
	 * Return all ingredients by page.
	 * 
	 * @param page
	 *            number page.
	 * @return list ingredient.
	 */
	public static PagedList<Ingredients> findAll(Integer page) {
		return findIngredients.query().setMaxRows(25).setFirstRow(25 * page).findPagedList();
		// return findIngredients.query().findList();
	}

	/**
	 * Check varieties list.
	 * 
	 * @param varitiesIngredient
	 *            varieties list.
	 */
	public void checkAndAddVarieties(List<Varieties> varitiesIngredient) {
		Varieties var = null;

		for (Varieties varietty : varitiesIngredient) {
			var = Varieties.findByName(varietty.getName());

			if (var == null) {
				this.addIngredientAndSave(varietty);

			} else if (var != null && !ingredientVarietyNameAddedList.contains(var.getName())) {

				varietyIngredientList.add(var);
				ingredientVarietyNameAddedList.add(var.getName());

			}
		}
	}

	/**
	 * If the ingredients no exist, create the new ingredient and save it in the
	 * list ingredients that set later to the recipe.
	 * 
	 * @param ing
	 *            ingredient.
	 */
	public void addIngredientAndSave(Varieties var) {
		var = new Varieties(var.getName(), var.getDescription());
		varietyIngredientList.add(var);
		ingredientVarietyNameAddedList.add(var.getName());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getCalories() {
		return calories;
	}

	public void setCalories(Integer calories) {
		this.calories = calories;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Long getConservationTime() {
		return conservationTime;
	}

	public void setConservationTime(Long conservationTime) {
		this.conservationTime = conservationTime;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Varieties> getVarieties() {
		return varieties;
	}

	public void setVarieties(List<Varieties> varieties) {
		this.varieties = varieties;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Recipes> getRecipes() {
		return recipes;
	}

	public void setRecipes(List<Recipes> recipes) {
		this.recipes = recipes;
	}

	public String getTypeIngredient() {
		return typeIngredient;
	}

	public void setTypeIngredient(String typeIngredient) {
		this.typeIngredient = typeIngredient;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "Ingredients [name=" + name + ", color=" + color + ", typeIngredient=" + typeIngredient
				+ ", description=" + description + ", quantity=" + quantity + ", calories=" + calories
				+ ", conservationTime=" + conservationTime + ", varieties=" + varieties + ", recipes=" + recipes + "]";
	}

}
