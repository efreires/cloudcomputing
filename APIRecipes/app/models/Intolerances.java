package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;

import io.ebean.Finder;
import io.ebean.Model;
import play.data.validation.Constraints.Required;

@Entity
public class Intolerances extends Model {

	@Id
	private Long id;
	@Required
	private String name;
	@ManyToMany(mappedBy = "intolerances")
	@JsonBackReference
	private List<Recipes> recipes = new ArrayList<>();

	public static final Finder<Long, Intolerances> findIntolerances = new Finder<>(Intolerances.class);

	/**
	 * Find intolerance by name.
	 * 
	 * @param name
	 *            name.
	 * @return intolerance.
	 */
	public static Intolerances findByName(String name) {
		return findIntolerances.query().where().eq("NAME", name).findOne();
	}

	/**
	 * Constructo with name param.
	 * 
	 * @param name
	 */
	public Intolerances(@Required String name) {
		super();
		this.name = name;
	}

	/**
	 * Constructor class.
	 * 
	 * @param id
	 *            id.
	 * @param name
	 *            name intolerances.
	 * @param recipes
	 *            list recipes.
	 */
	public Intolerances(Long id, @Required String name, List<Recipes> recipes) {
		super();
		this.id = id;
		this.name = name;
		this.recipes = recipes;
	}

	/**
	 * Constructor class.
	 * 
	 * @param name
	 *            name intolerances.
	 * @param recipes
	 *            list recipes.
	 */
	public Intolerances(@Required String name, List<Recipes> recipes) {
		super();
		this.name = name;
		this.recipes = recipes;
	}

	public Intolerances() {
		super();
	}

	public List<Recipes> getRecipes() {
		return recipes;
	}

	public void setRecipes(List<Recipes> recipes) {
		this.recipes = recipes;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Intolerances [name=" + name + ", recipes=" + recipes + "]";
	}

}
