package models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Preparation extends BaseModel {
	@Id
	private Long id;

	@OneToMany(cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<Steps> steps;

	private String difficulty;
	private Long preparationTime;

	@OneToOne(mappedBy = "preparation")
	@JsonBackReference
	private Recipes recipes;

	/**
	 * Constructor class.
	 * 
	 * @param steps
	 *            recipe steps.
	 * @param difficulty
	 *            recipe difficulty.
	 * @param preparationTime
	 *            reciper preparation.
	 * @param recipes
	 *            model recipe.
	 */
	public Preparation(List<Steps> steps, String difficulty, Long preparationTime, Recipes recipes) {
		super();
		this.steps = steps;
		this.difficulty = difficulty;
		this.preparationTime = preparationTime;
		this.recipes = recipes;
	}

	/**
	 * Default constructor.
	 */
	public Preparation() {
		super();
	}

	public List<Steps> getSteps() {
		return steps;
	}

	public void setSteps(List<Steps> steps) {
		this.steps = steps;
	}

	public Recipes getRecipes() {
		return recipes;
	}

	public void setRecipes(Recipes recipes) {
		this.recipes = recipes;
	}

	public String getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getPreparationTime() {
		return preparationTime;
	}

	public void setPreparationTime(Long preparationTime) {
		this.preparationTime = preparationTime;
	}

	@Override
	public String toString() {
		return "Preparation [steps=" + steps + ", difficulty=" + difficulty + ", preparationTime=" + preparationTime
				+ ", recipes=" + recipes + "]";
	}

}
