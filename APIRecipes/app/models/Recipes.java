package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import io.ebean.Ebean;
import io.ebean.Finder;
import io.ebean.PagedList;
import play.Logger;
import play.data.validation.Constraints.Min;
import play.data.validation.Constraints.Required;
import validators.CheckString;

@Entity
public class Recipes extends BaseModel {

	@Required
	@CheckString
	private String name;
	@CheckString(message = "Debe ser un String")
	private String description;
	private boolean privacity;

	@Min(1)
	private Integer diners;
	@OneToOne(cascade = CascadeType.ALL)
	@JsonManagedReference
	@Required
	private Preparation preparation;
	// private ImageIO Image;
	private int calories;
	@ManyToMany(cascade = CascadeType.ALL)
	@JsonManagedReference
	@Required
	private List<Ingredients> ingredients = new ArrayList<>();

	@Transient
	private List<Ingredients> ingredRecipe = new ArrayList<>();

	@ManyToMany(cascade = CascadeType.ALL)
	@JsonManagedReference
	@Required
	private List<Intolerances> intolerances = new ArrayList<>();

	@Transient
	private List<Intolerances> intoleRecipe = new ArrayList<>();

	@Transient
	private List<String> nameIntolerance = new ArrayList<>();
	@Transient
	private List<String> nameIngredient = new ArrayList<>();
	@Transient
	private Integer numberCalories = 0;

	/**
	 * Constructor default.
	 */
	public Recipes() {
		super();
	}

	/**
	 * Constructor class.
	 * 
	 * @param name
	 *            name recipe.
	 * @param description
	 *            description recipe.
	 * @param preparationTime
	 *            preparation recipe.
	 * @param diners
	 *            number of diners
	 * @param preparation
	 *            steps preparation.
	 * @param calories
	 *            number calories.
	 * @param ingredients
	 *            list ingredients.
	 */
	public Recipes(@Required String name, String description, @Min(1) Integer diners, Preparation preparation,
			int calories, List<Ingredients> ingredients, List<Intolerances> intolerances) {
		super();
		this.name = name;
		this.description = description;
		this.diners = diners;
		this.preparation = preparation;
		this.calories = calories;
		this.ingredients = ingredients;
		this.intolerances = intolerances;

	}

	/**
	 * Finder database recipes.
	 */
	public static final Finder<Long, Recipes> find = new Finder<>(Recipes.class);

	/**
	 * Finder database ingredients.
	 */
	public static final Finder<Long, Ingredients> findIngredients = new Finder<>(Ingredients.class);

	/**
	 * Find recipe by id.
	 * 
	 * @param id
	 *            id.
	 * @return recipe
	 */
	public static Recipes findById(Long id) {
		if (id == null) {
			throw new IllegalArgumentException();
		}

		return find.byId(id);
	}

	/**
	 * Find recipe by name.
	 * 
	 * @param name
	 *            name.
	 * @return recipe.
	 */
	public static Recipes findByName(String name) {

		return find.query().where().eq("NAME", name).findOne();
	}

	/**
	 * Find recipe by attribute.
	 * 
	 * @param attribute
	 *            attribute.
	 * @return List<Recipes>.
	 */
	public static List<Recipes> findByAttribute(String attr, String criteria, String val1) {
		if (criteria.equals("<"))
			return find.query().where().le(attr.toUpperCase(), val1).findList();
		else
			return find.query().where().gt(attr.toUpperCase(), val1).findList();
	}

	public static List<Recipes> searchByName(String word) {

		return find.query().where().contains("NAME", word).findList();
	}

	public static List<Recipes> searchByIngredientName(String name) {
		Ingredients ingredient = findIngredients.query().where().eq("NAME", name).findOne();
		return ingredient.getRecipes();
	}

	/**
	 * Find all recipes from the database.
	 * 
	 * @param page
	 *            page list.
	 * @return list of recipes.
	 */
	public static PagedList<Recipes> findAll(Integer page) {
		return find.query().setMaxRows(25).setFirstRow(25 * page).findPagedList();
	}

	public static List<Recipes> findAllRecipes() {

		return find.query().findList();

	}

	/**
	 * Update Recipe from the database.
	 * 
	 * @param recipe
	 */
	public static void updateRecipe(Recipes recipe) {

		Recipes.db().update(recipe);
	}

	/**
	 * Check if the recipe exist in the database, if no exist check the ingredient
	 * and and set the ingredient that no exist in the new recipe.
	 * 
	 * @param recipe
	 *            recipe.
	 * @return boolean.
	 */
	public boolean checkAndSaveRecipe(Recipes recipe) {
		List<Ingredients> ingredientRecipe = new ArrayList<>();
		List<Intolerances> intoleranceRecipes = new ArrayList<>();

		if (Recipes.findByName(recipe.name) == null) {

			ingredientRecipe.addAll(recipe.getIngredients());
			intoleranceRecipes.addAll(recipe.getIntolerances());

			checkAndAddIngredient(ingredientRecipe);
			checkAndAddIntolerance(intoleranceRecipes);

			this.setIngredients(ingredRecipe);
			this.setIntolerances(intoleRecipe);
			this.setCalories(numberCalories);
			this.save();
			return true;
		}

		return false;
	}

	/**
	 * Check if the ingredient exist in the list if the ingredient exist don't save
	 * this if not, save it
	 * 
	 * @param ingredientRecipe
	 *            list de ingredientes
	 */
	public void checkAndAddIngredient(List<Ingredients> ingredientRecipe) {
		Ingredients ingredient = null;

		for (Ingredients ing : ingredientRecipe) {
			ingredient = Ingredients.findByName(ing.getName());

			if (ingredient == null) {
				Logger.debug("las calorias bonitas en el for " + ing.getCalories());

				this.addIngredientAndSave(ing);

			} else if (ingredient != null && !nameIngredient.contains(((ingredient.getName())))) {

				if (ing.getCalories() != null)
					numberCalories += ing.getCalories();

				ingredRecipe.add(ingredient);
				nameIngredient.add(ingredient.getName());

			} else if (ing.getName().equalsIgnoreCase((ingredient.getName()))) {
			}
		}
	}

	/**
	 * Check if the Intolerance exist in the list if the Intolerance exist don't
	 * save this if not, save it
	 * 
	 * @param intoleranceRecipes
	 *            list de ingredientes
	 */
	public void checkAndAddIntolerance(List<Intolerances> intoleranceRecipes) {
		Intolerances intolerance = null;

		for (Intolerances into : intoleranceRecipes) {

			intolerance = Intolerances.findByName(into.getName());

			if (intolerance == null) {

				this.addIntoleranceAndSave(into);

			} else if (intolerance != null && !nameIntolerance.contains(((intolerance.getName())))) {

				intoleRecipe.add(intolerance);
				nameIntolerance.add(intolerance.getName());

			} else if (into.getName().equalsIgnoreCase((intolerance.getName()))) {
			}
		}
	}

	/**
	 * If the ingredients no exist, create the new ingredient and save it in the
	 * list ingredients that set later to the recipe.
	 * 
	 * @param ing
	 *            ingredient.
	 */
	public void addIngredientAndSave(Ingredients ing) {
		ing = new Ingredients(ing.getName(), ing.getColor(), ing.getTypeIngredient(), ing.getDescription(),
				ing.getQuantity(), ing.getCalories(), ing.getConservationTime(), ing.getVarieties());

		if (ing.getCalories() != null)
			numberCalories += ing.getCalories();
		ingredRecipe.add(ing);
		nameIngredient.add(ing.getName());

	}

	/**
	 * If the intolerance no exist, create the new intolerance and save it in the
	 * list of intolerance that set later to the recipe.
	 * 
	 * @param intolerance
	 *            intolerances.
	 */
	public void addIntoleranceAndSave(Intolerances intolerance) {
		intolerance = new Intolerances(intolerance.getName());
		intoleRecipe.add(intolerance);
		nameIntolerance.add(intolerance.getName());

	}

	public Preparation getPreparation() {
		return preparation;
	}

	public void setPreparation(Preparation preparation) {
		this.preparation = preparation;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getDiners() {
		return diners;
	}

	public void setDiners(Integer diners) {
		this.diners = diners;
	}

	public int getCalories() {
		return calories;
	}

	public void setCalories(int calories) {
		this.calories = calories;
	}

	public List<Ingredients> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Ingredients> ingredients) {
		this.ingredients = ingredients;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean isPrivacity() {
		return privacity;
	}

	public void setPrivacity(boolean privacity) {
		this.privacity = privacity;
	}

	public List<Intolerances> getIntolerances() {
		return intolerances;
	}

	public void setIntolerances(List<Intolerances> intolerances) {
		this.intolerances = intolerances;
	}

	@Override
	public String toString() {
		return "Recipes [name=" + name + ", description=" + description + ", privacity=" + privacity + ", diners="
				+ diners + ", preparation=" + preparation + ", calories=" + calories + ", ingredients=" + ingredients
				+ ", ingredRecipe=" + ingredRecipe + ", intolerances=" + intolerances + ", intoleRecipe=" + intoleRecipe
				+ "]";
	}

}
