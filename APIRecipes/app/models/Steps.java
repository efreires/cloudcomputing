package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;

import io.ebean.Finder;
import io.ebean.Model;
import play.api.mvc.Session;
import play.data.validation.Constraints.Required;

@Entity
public class Steps extends Model {

	@Id
	private Long id;

	@Required
	private String description;

	@Required
	private int stepNumber;

	@ManyToOne
	@JsonBackReference
	private Preparation preparation;

	/**
	 * Finder database steps.
	 */
	public static final Finder<Long, Steps> find = new Finder<>(Steps.class);

	/**
	 * Find step by id.
	 * 
	 * @param id
	 *            step id.
	 * @return object step.
	 */
	public static Steps findById(Long id) {
		if (id == null) {
			throw new IllegalArgumentException();
		}

		return find.byId(id);
	}

	/**
	 * Update step by id body.
	 * 
	 * @param step
	 *            object.
	 * @return boolean
	 */
	public static boolean updateStep(Steps step) {
		Steps stp = Steps.findById(step.getId());
		if (stp != null) {
			stp.setDescription(step.getDescription());
			stp.setStepNumber(step.getStepNumber());
			stp.update();
			return true;
		}
		return false;
	}

	/**
	 * Constructor class.
	 * 
	 * @param id
	 *            id step.
	 * @param description
	 *            description step.
	 * @param stepNumber
	 *            step number.
	 */
	public Steps(Long id, @Required String description, @Required int stepNumber) {
		super();
		this.id = id;
		this.description = description;
		this.stepNumber = stepNumber;
	}

	/**
	 * Default constructor.
	 */
	public Steps() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getStepNumber() {
		return stepNumber;
	}

	public void setStepNumber(int stepNumber) {
		this.stepNumber = stepNumber;
	}

	@Override
	public String toString() {
		return "Steps [id=" + id + ", description=" + description + ", stepNumber=" + stepNumber + "]";
	}

}
