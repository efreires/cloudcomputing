package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;

import io.ebean.Finder;
import io.ebean.Model;
import play.data.validation.Constraints.Required;

@Entity
public class Varieties extends Model {

	public static final Finder<Long, Varieties> findIngredients = new Finder<>(Varieties.class);

	@Id
	private Long id;

	@Required
	private String name;

	private String description;

	@ManyToMany(mappedBy = "varieties")
	@JsonBackReference
	private List<Ingredients> ingredients;

	/**
	 * Default constructor.
	 */
	public Varieties() {
		super();
	}

	/**
	 * Contructor class.
	 * 
	 * @param name
	 *            varietie name.
	 * @param description
	 *            varietie description.
	 */
	public Varieties(@Required String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}

	/**
	 * Find varietie by name
	 * 
	 * @param name
	 *            varietie name
	 * @return varietie object.
	 */
	public static Varieties findByName(String name) {
		return findIngredients.query().where().eq("NAME", name).findOne();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Ingredients> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Ingredients> ingredients) {
		this.ingredients = ingredients;
	}

	@Override
	public String toString() {
		return "Varieties [id=" + id + ", name=" + name + ", description=" + description + "]";
	}
}
