package utils;

public class Globals {

	// View

	public static final String XML = "application/xml";
	public static final String JSON = "application/json";
	public static final String HEADER_COUNT = "X-Count";

	// Cache
	public static final String ID = "id";
	public static final String KEY_INGREDIENT = "ingredient-";
	public static final String KEY_VIEW = "vista-";

	public static final String KEY_RECIPE = "receta-";

	// Request
	public static final String TYPE_OBJECT = "typeObject";
	public static final String ATTRIBUTE = "attribute";
	public static final String CRITERIA = "criteria";
	public static final String VALUE = "value";

	// Codes Messages
	public static final int codeSucces = 200;
	public static final int codeCreated = 201;
	public static final int codeNoExist = 404;
	public static final int codeNotAcceptable = 406;
	public static final int codeRepited = 409;
	public static final int codeUnsupported = 415;
	public static final int codeInternalServer = 500;
	public static final int codeNotImplemented = 501;

	// Keys Messages Recipes

	public static final String KeyRepited = "errorRepetided";
	public static final String KeyUpdated = "recipeUpdated";
	public static final String KeyDeleted = "recipeDeleted";
	public static final String KeyNoRecipes = "noRecipes";
	public static final String KeyNoExist = "noExist";
	public static final String KeyExist = "exist";
	public static final String KeyUnsupported = "unsupportedFormat";
	public static final String KeyUnsupportedSearch = "SearchNotSupported";
	public static final String KeyStepNoExist = "Step not found";
	public static final String KeySearchTypeNotSupported = "Attribute not valid";
	public static final String KeySucces = "Succes";
	public static final String KeyServerError = "serverError";
	public static final String keyNotImplemented = "notImplemented";
	// Keys Messages Ingredients

	public static final String KeyIngredientUpdated = "ingredientUpdated";
	public static final String KeyIngredientRepeated = "ingredientRepeated";
	public static final String KeyNoIngredients = "noIngredients";
	public static final String KeyNoExistIngredient = "noExistIngredient";
	public static final String KeyExistIngredient = "existIngredient";

	// Errors
	public static final String keyStringRequired = "typeString";

	// SearchController
	public static final String CALORIES = "CALORIES";
	public static final String CONSERVATION_TIME = "CONSERVATION_TIME";
	public static final String DINERS = "DINERS";
	public static final String RECIPES = "recipes";
	public static final String INGREDIENTS = "ingredients";

}
