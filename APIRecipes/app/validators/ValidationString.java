package validators;

import javax.validation.ConstraintValidator;

import play.Logger;
import play.data.validation.Constraints;
import play.libs.F;
import play.libs.F.Tuple;

public class ValidationString extends Constraints.Validator<String>
		implements ConstraintValidator<CheckString, String> {

	@Override
	public boolean isValid(String object) {
		try {
			Integer.valueOf(object);
			return false;

		} catch (Exception e) {
			Logger.error(e.getMessage());
			return true;
		}

	}

	@Override
	public Tuple<String, Object[]> getErrorMessageKey() {
		
	
		return new F.Tuple<String, Object[]>("The value must be a String", new Object[] { "" });
	}

	@Override
	public void initialize(CheckString constraintAnnotation) {
	}

}
