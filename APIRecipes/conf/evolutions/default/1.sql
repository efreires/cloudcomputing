# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table ingredients (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  color                         varchar(255),
  type_ingredient               varchar(255),
  description                   varchar(255),
  quantity                      varchar(255),
  calories                      integer,
  conservation_time             bigint,
  constraint pk_ingredients primary key (id)
);

create table ingredients_varieties (
  ingredients_id                bigint not null,
  varieties_id                  bigint not null,
  constraint pk_ingredients_varieties primary key (ingredients_id,varieties_id)
);

create table intolerances (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  constraint pk_intolerances primary key (id)
);

create table preparation (
  id                            bigint auto_increment not null,
  difficulty                    varchar(255),
  preparation_time              bigint,
  version                       bigint not null,
  when_created                  timestamp not null,
  when_updated                  timestamp not null,
  constraint pk_preparation primary key (id)
);

create table recipes (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  description                   varchar(255),
  privacity                     boolean default false not null,
  diners                        integer,
  preparation_id                bigint,
  calories                      integer not null,
  version                       bigint not null,
  when_created                  timestamp not null,
  when_updated                  timestamp not null,
  constraint uq_recipes_preparation_id unique (preparation_id),
  constraint pk_recipes primary key (id)
);

create table recipes_ingredients (
  recipes_id                    bigint not null,
  ingredients_id                bigint not null,
  constraint pk_recipes_ingredients primary key (recipes_id,ingredients_id)
);

create table recipes_intolerances (
  recipes_id                    bigint not null,
  intolerances_id               bigint not null,
  constraint pk_recipes_intolerances primary key (recipes_id,intolerances_id)
);

create table steps (
  id                            bigint auto_increment not null,
  description                   varchar(255),
  step_number                   integer not null,
  preparation_id                bigint,
  constraint pk_steps primary key (id)
);

create table varieties (
  id                            bigint auto_increment not null,
  name                          varchar(255),
  description                   varchar(255),
  constraint pk_varieties primary key (id)
);

alter table ingredients_varieties add constraint fk_ingredients_varieties_ingredients foreign key (ingredients_id) references ingredients (id) on delete restrict on update restrict;
create index ix_ingredients_varieties_ingredients on ingredients_varieties (ingredients_id);

alter table ingredients_varieties add constraint fk_ingredients_varieties_varieties foreign key (varieties_id) references varieties (id) on delete restrict on update restrict;
create index ix_ingredients_varieties_varieties on ingredients_varieties (varieties_id);

alter table recipes add constraint fk_recipes_preparation_id foreign key (preparation_id) references preparation (id) on delete restrict on update restrict;

alter table recipes_ingredients add constraint fk_recipes_ingredients_recipes foreign key (recipes_id) references recipes (id) on delete restrict on update restrict;
create index ix_recipes_ingredients_recipes on recipes_ingredients (recipes_id);

alter table recipes_ingredients add constraint fk_recipes_ingredients_ingredients foreign key (ingredients_id) references ingredients (id) on delete restrict on update restrict;
create index ix_recipes_ingredients_ingredients on recipes_ingredients (ingredients_id);

alter table recipes_intolerances add constraint fk_recipes_intolerances_recipes foreign key (recipes_id) references recipes (id) on delete restrict on update restrict;
create index ix_recipes_intolerances_recipes on recipes_intolerances (recipes_id);

alter table recipes_intolerances add constraint fk_recipes_intolerances_intolerances foreign key (intolerances_id) references intolerances (id) on delete restrict on update restrict;
create index ix_recipes_intolerances_intolerances on recipes_intolerances (intolerances_id);

alter table steps add constraint fk_steps_preparation_id foreign key (preparation_id) references preparation (id) on delete restrict on update restrict;
create index ix_steps_preparation_id on steps (preparation_id);


# --- !Downs

alter table ingredients_varieties drop constraint if exists fk_ingredients_varieties_ingredients;
drop index if exists ix_ingredients_varieties_ingredients;

alter table ingredients_varieties drop constraint if exists fk_ingredients_varieties_varieties;
drop index if exists ix_ingredients_varieties_varieties;

alter table recipes drop constraint if exists fk_recipes_preparation_id;

alter table recipes_ingredients drop constraint if exists fk_recipes_ingredients_recipes;
drop index if exists ix_recipes_ingredients_recipes;

alter table recipes_ingredients drop constraint if exists fk_recipes_ingredients_ingredients;
drop index if exists ix_recipes_ingredients_ingredients;

alter table recipes_intolerances drop constraint if exists fk_recipes_intolerances_recipes;
drop index if exists ix_recipes_intolerances_recipes;

alter table recipes_intolerances drop constraint if exists fk_recipes_intolerances_intolerances;
drop index if exists ix_recipes_intolerances_intolerances;

alter table steps drop constraint if exists fk_steps_preparation_id;
drop index if exists ix_steps_preparation_id;

drop table if exists ingredients;

drop table if exists ingredients_varieties;

drop table if exists intolerances;

drop table if exists preparation;

drop table if exists recipes;

drop table if exists recipes_ingredients;

drop table if exists recipes_intolerances;

drop table if exists steps;

drop table if exists varieties;

