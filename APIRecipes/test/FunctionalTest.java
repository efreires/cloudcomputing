import org.junit.Test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import models.Ingredients;
import models.Recipes;
import play.mvc.Http.RequestBuilder;
import play.Logger;
import play.mvc.Result;
import play.test.Helpers;
import play.test.WithApplication;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * A functional test starts a Play application for every test.
 *
 * https://www.playframework.com/documentation/latest/JavaFunctionalTest
 */
public class FunctionalTest extends WithApplication {

	@Test
	public void getAllRecipesWithoutPagination() {
		RequestBuilder req = Helpers.fakeRequest().method("GET").uri("/recipes").header("Accept", "application/json");

		Result r = Helpers.route(app, req);
		Logger.debug("Request sin paginación" + r.status());
		assertThat(r.status()).isEqualTo(404);
	}

	@Test
	public void getIngredientsAllWithoutPagination() {
		RequestBuilder req = Helpers.fakeRequest().method("GET").uri("/ingredients").header("Accept",
				"application/json");

		Result r = Helpers.route(app, req);
		Logger.debug("Request sin paginación" + r.status());
		assertThat(r.status()).isEqualTo(404);
	}

	@Test
	public void getAlIngredientsWithPagination() {
		RequestBuilder req = Helpers.fakeRequest().method("GET").uri("/ingredients/0").header("Accept",
				"application/json");

		Result r = Helpers.route(app, req);
		Logger.debug("Request con paginación" + r.status());
		assertThat(r.status()).isEqualTo(200);
	}

	@Test
	public void postCreateRecipe() throws Exception {
		String json = "{ \"name\":\"Eladio\", \"description\":\"yes\", \"preparation\":{ \"preparationTime\":10000, \"level\":\"facil\", \"steps\":[{\"stepNumber\":1, \"description\":\"si\"},{\"stepNumber\":2, \"description\":\"no\"},{\"stepNumber\":5, \"description\":\"asdasd\"}] }, \"diners\":1, \"intolerances\":[ { \"name\":\"gluten\" } ], \"ingredients\":[ { \"name\":\"yyttt\", \"color\":\"gris\", \"conservationTime\":7647647, \"description\":\"es so no lo es\", \"calories\":123, \"varieties\":[ { \"name\":\"deljojojojo\", \"description\":\"jojojojo eso\" } ], \"typeIngredient\":\"conservas\" } ] }] }";
		JsonNode jsonNode = (new ObjectMapper()).readTree(json);
		RequestBuilder req = Helpers.fakeRequest().method("POST").uri("/recipe").header("Accept", "application/json")
				.bodyJson(jsonNode);
		Result r = Helpers.route(app, req);
		assertThat(r.status()).isEqualTo(201);
	}

	@Test
	public void postCreateIngredient() throws Exception {
		String json = "{ \"name\":\"Nacho\", \"description\":\"yes\", \"preparation\":{ \"preparationTime\":10000, \"level\":\"facil\", \"steps\":[{\"stepNumber\":1, \"description\":\"si\"},{\"stepNumber\":2, \"description\":\"no\"},{\"stepNumber\":5, \"description\":\"asdasd\"}] }, \"diners\":1, \"intolerances\":[ { \"name\":\"gluten\" } ], \"ingredients\":[ { \"name\":\"yyttt\", \"color\":\"gris\", \"conservationTime\":7647647, \"description\":\"es so no lo es\", \"calories\":123, \"varieties\":[ { \"name\":\"deljojojojo\", \"description\":\"jojojojo eso\" } ], \"typeIngredient\":\"conservas\" } ] }] }";
		JsonNode jsonNode = (new ObjectMapper()).readTree(json);
		RequestBuilder req = Helpers.fakeRequest().method("POST").uri("/recipe").header("Accept", "application/json")
				.bodyJson(jsonNode);
		Result r = Helpers.route(app, req);
		assertThat(r.status()).isEqualTo(201);
	}

	@Test
	public void getRecipesBySearch() {
		RequestBuilder req = Helpers.fakeRequest().method("GET")
				.uri("/search?typeObject=recipes&attribute=diners&criteria=less&value=1234")
				.header("Accept", "application/json");

		Result r = Helpers.route(app, req);
		Logger.debug("Request search" + r.status());
		assertThat(r.status()).isEqualTo(200);
	}

	@Test
	public void testModelRecipes() {
		Recipes m = Recipes.findByName("nachos");
		assertThat(m).isNull();
	}

	@Test
	public void testModelIngredients() {
		Ingredients m = Ingredients.findByName("eladios");
		assertThat(m).isNull();
	}
}
