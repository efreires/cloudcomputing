import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

import controllers.SearchController;
import models.Recipes;
import play.test.WithApplication;
import utils.Globals;

/**
 * Unit testing does not require Play application start up.
 *
 * https://www.playframework.com/documentation/latest/JavaTest
 */
public class UnitTest extends WithApplication {

	@Test
	public void checkAndSaveRecipeTest() {
		Recipes reci = new Recipes();

		assertThat(reci.checkAndSaveRecipe(reci)).isFalse();
	}

	@Test
	public void checkValidateData() {
		String check = "Hola JM";

		assertThat(SearchController.validateData(check)).isFalse();
	}

	@Test
	public void checkValidateColumns() {

		assertThat(SearchController.validateColumns(Globals.CALORIES)).isTrue();
	}
}
