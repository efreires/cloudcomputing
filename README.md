Proyecto de Eladio Freire
# Routes


# List recipes

GET  /recipes/:page				controllers.RecipesController.retrieveListRecipes(page:Integer)

#Get recipe by name

GET  /recipe/:name			controllers.RecipesController.retrieveRecipe(name:String) 

#Get recipe by ingredient

GET  /recipe/ingredient/:ingredient			controllers.RecipesController.retrieveRecipeIngredient(ingredient:String) 

#Add recipe

POST  /recipe				controllers.RecipesController.addRecipe()

# Edit recipe

PUT  /recipe/:name		controllers.RecipesController.updateRecipe(name:String) 

# Delete recipe

DELETE  /recipe/:name				controllers.RecipesController.deleteRecipe(name:String) 

#Get ingredient by name

GET  /ingredient/:name			controllers.IngredientController.retrieveIngredient(name:String) 

#Add ingredient

POST  /ingredient				controllers.IngredientController.addIngredient()

# Update ingredient

PUT  /ingredient/:name				controllers.IngredientController.updateIngredient(name: String) 

# GET List Ingredients

GET /ingredients/:page				controllers.IngredientController.retrieveIngredients(page:Integer)

# Search 

GET /search						controllers.SearchController.searchByCriteria()


# Steps Resources

PUT /step 	controllers.StepController.updateStepDescription()